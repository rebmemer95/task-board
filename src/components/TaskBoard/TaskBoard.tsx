import React, { useCallback, useEffect, useMemo, useState } from 'react';

import {
  Task,
  TaskBoardProps,
  TaskColumnsItemProps,
} from '../../validators/validators';
import TaskColumn from '../TaskColumn/TaskColumn';
import './TaskBoard.scss';

const TaskBoard: React.FC<TaskBoardProps> = ({ tasks }) => {
  const [taskColumns, setTaskColumns] = useState<Array<TaskColumnsItemProps>>(
    []
  );
  const [currentColumn, setCurrentColumn] = useState<TaskColumnsItemProps>({
    id: 0,
    title: '',
    items: [],
  });
  const [currentTask, setCurrentTask] = useState<Task>({
    number: '',
    title: '',
    firstName: '',
    lastName: '',
    status: '',
    importance: '',
    date: '',
  });

  const groupedTasks: TaskColumnsItemProps[] = useMemo(() => {
    return [
      {
        id: 1,
        title: 'planned',
        items: tasks ? tasks.filter((task) => task.status === 'PLANNED') : [],
      },
      {
        id: 2,
        title: 'in progress',
        items: tasks
          ? tasks.filter((task) => task.status === 'IN PROGRESS')
          : [],
      },
      {
        id: 3,
        title: 'testing',
        items: tasks ? tasks.filter((task) => task.status === 'TESTING') : [],
      },
      {
        id: 4,
        title: 'done',
        items: tasks ? tasks.filter((task) => task.status === 'DONE') : [],
      },
    ];
  }, [tasks]);

  useEffect(() => {
    setTaskColumns(groupedTasks);
  }, [groupedTasks]);

  const handleDragOver: (event: any) => void = useCallback((event) => {
    event.preventDefault();
    event.currentTarget.classList.add('task-board--active');
  }, []);

  const handleLeave: (event: any) => void = useCallback((event) => {
    event.currentTarget.classList.remove('task-board--active');
  }, []);

  const handleDrop: (event: any, column: TaskColumnsItemProps) => void =
    useCallback(
      (event, column) => {
        event.preventDefault();
        event.currentTarget.classList.remove('task-board--active');
        column.items.push(currentTask);
        currentTask.status = column.title.toUpperCase();
        const currentColumnItems: Array<Task> = currentColumn.items;
        const currentColumnIndex: number =
          currentColumnItems?.indexOf(currentTask);
        currentColumnItems?.splice(currentColumnIndex, 1);
        setTaskColumns(
          taskColumns.map((item) => {
            if (item.id === column.id) return column;
            if (item.id === currentColumn.id) return currentColumn;
            return item;
          })
        );
      },
      [currentTask, currentColumn, taskColumns]
    );

  return (
    <div className="task-board">
      {taskColumns &&
        taskColumns.map((column, key) => {
          return (
            <TaskColumn
              key={key + column.id}
              tasks={column.items}
              title={column.title}
              column={column}
              setCurrentColumn={setCurrentColumn}
              setCurrentTask={setCurrentTask}
              handleDragOver={handleDragOver}
              handleDrop={handleDrop}
              handleLeave={handleLeave}
            />
          );
        })}
    </div>
  );
};

export default TaskBoard;
