import React from 'react';
import renderer from 'react-test-renderer';
import TaskBoard from './TaskBoard';

test('TaskBoard renders correctly', () => {
  const component = renderer.create(<TaskBoard />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
