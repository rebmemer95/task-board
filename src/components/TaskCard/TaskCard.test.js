import React from 'react';
import renderer from 'react-test-renderer';
import TaskCard from './TaskCard';

test('TaskCard renders correctly', () => {
  const component = renderer.create(<TaskCard />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
