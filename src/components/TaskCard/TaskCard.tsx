import React from 'react';

import {
  Task,
  TaskCardProps,
  TaskColumnsItemProps,
} from '../../validators/validators';
import './TaskCard.scss';

const TaskCard: React.FC<TaskCardProps> = React.memo(
  ({ task, column, setCurrentColumn, setCurrentTask }) => {
    const getTaskCard: React.JSXElementConstructor<Task> = (currentTask) => {
      let taskCardClassName = 'task-card';

      if (currentTask) {
        switch (currentTask.importance.toUpperCase()) {
          case 'MUST':
            taskCardClassName = taskCardClassName + ' task-card--must';
            break;
          case 'SHOULD':
            taskCardClassName = taskCardClassName + ' task-card--should';
            break;
          default:
            taskCardClassName = taskCardClassName + ' task-card--could';
            break;
        }
      }

      return (
        <div className={taskCardClassName}>
          <div>{currentTask?.number}</div>
          <div>{currentTask?.title}</div>
          <div>{currentTask?.firstName}</div>
        </div>
      );
    };

    const handleDragStart = (column: TaskColumnsItemProps, task: Task) => {
      setCurrentColumn(column);
      setCurrentTask(task);
    };

    return (
      <div
        className="task-card__container"
        onDragStart={() => handleDragStart(column, task)}
        draggable={true}
      >
        {getTaskCard(task)}
      </div>
    );
  }
);

export default TaskCard;
