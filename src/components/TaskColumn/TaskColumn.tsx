import React from 'react';

import { Task, TaskColumnProps } from '../../validators/validators';
import TaskCard from '../TaskCard/TaskCard';
import { importancePriority } from '../../data/tasks';
import './TaskColumn.scss';

const sortByName = (item1: Task, item2: Task) => {
  const item1Name: string = item1.firstName.toLowerCase();
  const item2Name: string = item2.firstName.toLowerCase();
  if (item1Name < item2Name) return -1;
  if (item1Name > item2Name) return 1;
  return 0;
};

const sortByImportance = (item1: Task, item2: Task) => {
  const item1Importance: string = item1.importance.toUpperCase();
  const item2Importance: string = item2.importance.toUpperCase();
  const item1ImportancePriority: number = importancePriority[item1Importance];
  const item2ImportancePriority: number = importancePriority[item2Importance];

  if (item1ImportancePriority < item2ImportancePriority) return -1;
  if (item1ImportancePriority > item2ImportancePriority) return 1;
  return 0;
};

const TaskColumn: React.FC<TaskColumnProps> = ({
  title,
  tasks,
  column,
  setCurrentColumn,
  setCurrentTask,
  handleDragOver,
  handleDrop,
  handleLeave,
}) => {
  const getTitleBlock: React.JSXElementConstructor<any> = (
    columnTitle: string
  ) => {
    let titleBlockClassName: string = 'task-column__title';

    switch (columnTitle && columnTitle.toUpperCase()) {
      case 'IN PROGRESS':
        titleBlockClassName =
          titleBlockClassName + ' task-column__title--in-progress';
        break;
      case 'TESTING':
        titleBlockClassName =
          titleBlockClassName + ' task-column__title--testing';
        break;
      case 'DONE':
        titleBlockClassName = titleBlockClassName + ' task-column__title--done';
        break;
      default:
        titleBlockClassName =
          titleBlockClassName + ' task-column__title--planned';
        break;
    }

    return <div className={titleBlockClassName}>{columnTitle}</div>;
  };

  return (
    <section
      className="task-column"
      onDragOver={(event) => handleDragOver(event)}
      onDrop={(event) => handleDrop(event, column)}
      onDragLeave={(event) => handleLeave(event)}
    >
      {getTitleBlock(title)}
      {tasks &&
        tasks
          .sort(sortByImportance)
          .sort(sortByName)
          .map((task, key) => {
            return (
              <TaskCard
                key={key}
                task={task}
                column={column}
                setCurrentColumn={setCurrentColumn}
                setCurrentTask={setCurrentTask}
              />
            );
          })}
    </section>
  );
};

export default TaskColumn;
