import React from 'react';
import renderer from 'react-test-renderer';
import TaskColumn from './TaskColumn';

test('TaskColumn renders correctly', () => {
  const component = renderer.create(<TaskColumn />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
