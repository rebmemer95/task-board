import MainPage from './pages/mainPage';
import './App.scss';

import { tasks } from './data/tasks';

const App: React.FC = () => {
  return (
    <div className="App">
      <MainPage tasks={tasks} />
    </div>
  );
};

export default App;
