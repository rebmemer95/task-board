export interface Task {
  number: string;
  title: string;
  firstName: string;
  lastName: string;
  status: string;
  importance: string;
  date: string;
}

export interface TaskBoardProps {
  tasks: Array<Task>;
}

export interface TaskColumnsItemProps {
  id: number;
  title: string;
  items: Array<Task>;
}

export interface TaskCardProps {
  task: Task;
  column: TaskColumnsItemProps;
  setCurrentColumn: (
    value:
      | TaskColumnsItemProps
      | ((prevVar: TaskColumnsItemProps) => TaskColumnsItemProps)
  ) => void;
  setCurrentTask: (value: Task | ((prevVar: Task) => Task)) => void;
}

export interface TaskColumnProps {
  title: string;
  tasks: Array<Task>;
  column: TaskColumnsItemProps;
  setCurrentColumn: (
    value:
      | TaskColumnsItemProps
      | ((prevVar: TaskColumnsItemProps) => TaskColumnsItemProps)
  ) => void;
  setCurrentTask: (value: Task | ((prevVar: Task) => Task)) => void;
  handleDragOver: (event: any) => void;
  handleDrop: (event: any, column: TaskColumnsItemProps) => void;
  handleLeave: (event: any) => void;
}
