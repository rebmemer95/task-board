export const tasks: {
  number: string;
  title: string;
  firstName: string;
  lastName: string;
  status: string;
  importance: string;
  date: string;
}[] = [
  {
    number: 'TSK-0001',
    title: 'Task 1',
    firstName: 'Ivan',
    lastName: 'Ivanov',
    status: 'PLANNED',
    importance: 'MUST',
    date: '4.09.2021',
  },
  {
    number: 'TSK-0002',
    title: 'Task 2',
    firstName: 'Boris',
    lastName: 'Ivanov',
    status: 'IN PROGRESS',
    importance: 'SHOULD',
    date: '4.09.2021',
  },
  {
    number: 'TSK-0003',
    title: 'Task 3',
    firstName: 'Ivan',
    lastName: 'Ivanov',
    status: 'TESTING',
    importance: 'COULD',
    date: '4.09.2021',
  },
  {
    number: 'TSK-0004',
    title: 'Task 4',
    firstName: 'Ivan',
    lastName: 'Ivanov',
    status: 'DONE',
    importance: 'MUST',
    date: '4.09.2021',
  },
  {
    number: 'TSK-0005',
    title: 'Task 5',
    firstName: 'Boris',
    lastName: 'Ivanov',
    status: 'PLANNED',
    importance: 'SHOULD',
    date: '4.09.2021',
  },
  {
    number: 'TSK-0006',
    title: 'Task 6',
    firstName: 'Ivan',
    lastName: 'Ivanov',
    status: 'PLANNED',
    importance: 'COULD',
    date: '4.09.2021',
  },
  {
    number: 'TSK-0007',
    title: 'Task 7',
    firstName: 'Boris',
    lastName: 'Ivanov',
    status: 'IN PROGRESS',
    importance: 'SHOULD',
    date: '4.09.2021',
  },
  {
    number: 'TSK-0008',
    title: 'Task 8',
    firstName: 'Ivan',
    lastName: 'Ivanov',
    status: 'TESTING',
    importance: 'MUST',
    date: '4.09.2021',
  },
  {
    number: 'TSK-0009',
    title: 'Task 9',
    firstName: 'Boris',
    lastName: 'Ivanov',
    status: 'DONE',
    importance: 'MUST',
    date: '4.09.2021',
  },
  {
    number: 'TSK-0010',
    title: 'Task 10',
    firstName: 'Boris',
    lastName: 'Ivanov',
    status: 'PLANNED',
    importance: 'MUST',
    date: '4.09.2021',
  },
];

export const importancePriority: {
  MUST: number;
  SHOULD: number;
  COULD: number;
} = {
  MUST: 0,
  SHOULD: 1,
  COULD: 2,
};
