import TaskBoard from '../components/TaskBoard/TaskBoard';
import './mainPage.scss';

interface Task {
  number: string;
  title: string;
  firstName: string;
  lastName: string;
  status: string;
  importance: string;
  date: string;
}

interface TaskBoardProps {
  tasks: Array<Task>;
}

const MainPage: React.FC<TaskBoardProps> = ({ tasks }) => {
  return (
    <div className="main-page">
      <header className="main-page__header">task board</header>
      <main className="main-page__main-content">
        <TaskBoard tasks={tasks} />
      </main>
    </div>
  );
};

export default MainPage;
